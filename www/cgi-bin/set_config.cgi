#!/bin/sh
echo "Content-type: text/html"
echo ""
echo '<html>'
echo '<head>'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<link rel="stylesheet" href="../css/pure-min.css" media="screen">'
echo '<title>JADOS</title>'
echo '</head>'
echo '<body style="font-size:12pt;margin-left:20px;margin-top:20px">'
echo '<br><br>'
echo '<pre>'
# Leemos el valor enviado mediante post y se guarda en la variable cadena
read -t 1 cadena
func=${cadena:0:3}
if [ "$func" != "tco" ]; then
cat <<EOF
<div id="carga" align="center">
  <img src="../images/saving.gif" />
</div>
EOF
fi
# Cargamos variables
hw_id=$(/etc/daemon hardware_id)
device=$(uci get wireless.@wifi-iface[0].device)
model=$(cat /var/sysinfo/board_name)
salir="true"
if [ "$func" == "usb" ]; then
    gpio=$(ls /sys/class/gpio/ | grep -Eo "gpio[0-9]")
    if [ $value == "encendido" ]; then
        echo 1 > /sys/class/gpio/"$gpio"/value
    else
        echo 0 > /sys/class/gpio/"$gpio"/value
    fi
fi
if [ "$func" == "psw" ]; then
    value=$(echo $cadena | cut -f1 -d '&'| sed s/'+'/' '/g)
	pass=${value:4:60}
	
	value=$(echo $cadena | cut -f2 -d '&' | sed s/'%27'/''/g)
	ifacename=${value:4:60}
	uci set wireless."$ifacename".key="$pass"
	uci commit wireless
cat <<EOF
			<script type="text/javascript">
			<!--
    		var x;
    		if (confirm("Desea aplicar los cambios!") == true) {
        	script = document.createElement('script');
        	script.src = 'net_restart.cgi';
        	script.type = "text/javascript";
        	document.getElementsByTagName('head')[0].appendChild(script);
        	document.getElementsByTagName('head')[0].removeChild(script);
    		} else {
        	x = "cancel";
    		};
			//-->
			</script>
EOF
fi
if [ "$func" == "svp" ]; then

	killall openvpn
	sleep 2
	echo '<script type="text/javascript">'                                     
	echo '<!--'                                              
	echo 'window.location = "vpn.hs"'                  
	echo '//-->'                                  
	echo '</script>'
fi
if [ "$func" == "dvp" ]; then
    value=$(echo $cadena | cut -f1 -d '&')
	value=${value:4:45}
	rm /vpn/$value
	sleep 1
	echo '<script type="text/javascript">'                                     
	echo '<!--'                                              
	echo 'window.location = "vpn.hs"'                  
	echo '//-->'                                  
	echo '</script>'
fi
if [ "$func" == "vpn" ]; then
	value=${cadena:4:25}
	echo $value > /usr/vpn
	/etc/daemon normal_conn &
	sleep 10
	echo '<script type="text/javascript">'                                     
	echo '<!--'                                              
	echo 'window.location = "vpn.hs"'                  
	echo '//-->'                                  
	echo '</script>'
fi
if [ "$func" == "nom" ]; then
    value=$(echo $cadena | cut -f1 -d '&')
	value=${value:4:25}
    uci set wireless.@wifi-iface[0].ssid=$value
			
	value=$(echo $cadena | cut -f2 -d '&')
	value=${value:4:25}
    uci set wireless.$device.hwmode=$value
		
	value=$(echo $cadena | cut -f3 -d '&')
	value=${value:4:25}
	uci set wireless.$device.channel=$value

	value=$(echo $cadena | cut -f4 -d '&')
	value=${value:4:25}
	uci set wireless.@wifi-iface[0].encryption=$value
		
	value=$(echo $cadena | cut -f5 -d '&')
	value=${value:4:25}	
	uci set wireless.@wifi-iface[0].key=$value
		
	uci commit wireless
fi      
if [ "$func" == "tco" ]; then
	value=$(echo $cadena | cut -f1 -d '&')
	value=${value:4:25}
	
	if [ "$value" == "static" ]; then
		echo "static" > /usr/imode
		uci set network.wan.proto=$value

		value=$(echo $cadena | cut -f2 -d '&')
		value=${value:4:25}
		uci set network.wan.ipaddr=$value
	
		value=$(echo $cadena | cut -f3 -d '&')
		value=${value:4:25}
		uci set network.wan.netmask=$value
	
		value=$(echo $cadena | cut -f4 -d '&')
		value=${value:4:25}
		uci set network.wan.gateway=$value
	
		uci commit network
	fi
	if [ "$value" == "dhcp" ]; then
		echo "dhcp" > /usr/imode
		uci set network.wan.proto='dhcp'
	fi
	if [ $(echo "$value" | grep -o "+" | wc -l) == 5 ]; then
		salir="false"
		iface=$(uci add wireless wifi-iface)
		value=$(echo $cadena | sed s/'%26%26'/';'/g | sed s/'+'/':'/g )
		value=${value:4:200}
		
		uci set wireless."$iface".mode="sta"
		uci set wireless."$iface".network="wan2"
		uci set wireless."$iface".device="radio0"
		
		mac=$(echo $value | cut -f1 -d ';')
		uci set wireless."$iface".bssid="$mac"

		ssid=$(echo $value | cut -f2 -d ';')
		echo "$ssid" > /usr/imode
		uci set wireless."$iface".ssid="$ssid"
		
		sec=$(echo $value | cut -f3 -d ';')
		uci set wireless."$iface".encryption="$sec"
		
		cha=$(echo $value | cut -f4 -d ';')
		uci set wireless."$device".channel="$cha"

		if [ "$sec" != "none" ]; then
cat <<EOF
		<form action="set_config.cgi" method="post" class="pure-form pure-form-stacked">   
		<fieldset>
		<div class="pure-control-group" align="left">
			<label for="pass_rep" style="width: 150px">Ingrese contraseña de red inal&aacute;mbrica</label>
			<input type="password" name="psw" id="pass_rep" style="width: 200px" required/>
			<input type="hidden" name="ifa" value="'$iface'">
		</div>
		<div class="pure-controls">
			<button type="submit" class="pure-button pure-button-primary" style="width: 200px">Continuar</button>
		</div>
		</fieldset>
		</form>
EOF
		else
		uci commit wireless
cat <<EOF
		<script type="text/javascript">
		<!--
    		var x;
    		if (confirm("Desea aplicar los cambios!") == true) {
        	script = document.createElement('script');
        	script.src = 'net_restart.cgi';
        	script.type = "text/javascript";
        	document.getElementsByTagName('head')[0].appendChild(script);
        	document.getElementsByTagName('head')[0].removeChild(script);
    		} else {
        	x = "cancel";
    	};
		//-->
		</script>
EOF
		fi
	fi
fi
if [ "$func" == "ipl" ]; then
	value=$(echo $cadena | cut -f1 -d '&')
	value=${value:4:25}
	uci set network.lan.ipaddr=$value
	
	value=$(echo $cadena | cut -f2 -d '&')
	value=${value:4:25}
	uci set network.lan.netmask=$value
	
	value=$(echo $cadena | cut -f3 -d '&')
	value=${value:4:25}
	uci set network.lan.gateway=$value
	
	value=$(echo $cadena | cut -f4 -d '&')
	dns1=${value:4:25}
	value=$(echo $cadena | cut -f5 -d '&')
	dns2=${value:4:25}
	uci set network.lan.dns=$dns1" "$dns2
	
	uci commit network
fi
if [ "$func" == "ndi" ]; then
	value=$(echo $cadena | cut -f1 -d '&')
	value=${value:4:25}
	uci set system.@system[0].hostname=$value
	
	value=$(echo $cadena | cut -f2 -d '&')
	value=${value:4:25}
	uci set system.ntp.server=$value
	
	value=$(echo $cadena | cut -f3 -d '&')
	value=${value:4:50}
	hash=`echo -n "admin:website:$value" | md5sum | cut -b -32`
	echo /usr/htdigest.user > admin:website:$hash
	
	uci commit system
fi
if [ "$func" == "led" ]; then
	    value=${a:4:25}
        if [ "$value" == "noche" ]; then
            for i in /sys/class/leds/* ; do echo 0 > "$i"/brightness ; done
			echo "" > /tmp/led_off
        else
            /etc/init.d/led restart
			rm /tmp/led_off
        fi
fi
sleep 1
if [ "$salir" == "true" ]; then
echo '<script type="text/javascript">'
echo '<!--'
echo 'window.location = "settings.cgi"'
echo '//-->'
echo '</script>'
echo '</body>'
echo '</html>'
fi
exit 0