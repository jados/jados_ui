#!/usr/bin/haserl --upload-limit=8192 --upload-dir=/tmp 
content-type: text/html

<html>
<head>
<link rel="stylesheet" href="../css/pure-min.css">
<script type="text/javascript" src="../js/valida_bin.js"></script>
</head>
<body>
<br>
<form action="<% echo -n $SCRIPT_NAME %>" class="pure-form pure-form-stacked" method=POST enctype="multipart/form-data" >
<fieldset>
<label for="file">Seleccione firmware</label>
<input type=file id=file name=uploadfile>
<input type=submit class="pure-button pure-button-primary" style="width: 130px" value=Aceptar>
</fieldset>
</form>
<% if [ -f /tmp/upgrade.bin ]; then
rm /tmp/upgrade.bin
fi %>
<br>
<h4>Estado: <span id=status>Sin tareas activas</span></h4>
<br>
<% if test -n "$HASERL_uploadfile_path"; then %>
<h3>Archivo enviado: <% echo -n $FORM_uploadfile_name %></h3>
<% mv -f "$HASERL_uploadfile_path" "/tmp/upgrade.bin"  %>
<script type="text/javascript">
function update(){
        script = document.createElement('script');
        script.src = 'flash.cgi';
        script.type = "text/javascript";
        document.getElementsByTagName('head')[0].appendChild(script);
        // Now get rid of the element so as not to create multiple iterations  
        document.getElementsByTagName('head')[0].removeChild(script);
}
if (confirm("Actualizar firmware") == true) {	    
	    document.getElementById("status").innerHTML = "Actualizando..."
	    update()
    } else {
        document.getElementById("status").innerHTML = "Cancelado"
        window.location = "update.hs"
    };
</script>
<% fi %>
</body></html>