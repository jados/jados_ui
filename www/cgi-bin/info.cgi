#!/bin/sh
echo "Content-type: text/html"
echo ""
echo '<html>'
echo '<head>'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<title></title>'
echo '</head>'
echo '<body style="font-size:12pt;margin-left:5px;margin-top:5px">'
echo '<pre>'
hw_id=$(/etc/daemon hardware_id)
version=$(/etc/daemon version)
hw_model=$(cat /tmp/sysinfo/model)
hostname=$(uname -n)
echo '<font color="#000000">'
echo "Nombre de dispositivo: $hostname"
echo "Identificador: $hw_id"
echo "Modelo: $hw_model"
echo "Versión de Software: $version"
echo '</font>'
echo '</pre>'
echo '</body>'
echo '</html>'
exit 0
