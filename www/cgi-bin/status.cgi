#!/bin/sh
echo "Content-type: text/html"
echo ""
echo '<html>'
echo '<head>'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<title></title>'
echo '</head>'
echo '<body style="font-size:12pt;margin-left:0px;margin-top:2px">'
echo '<font color="#FFFFFF">'  
if eval "ping -w 1 -W 4 8.8.8.8 > /dev/null"; then
    echo ' <img src="../images/net_on.png" title="Conectado a Internet" height="32" width="32">'
fi
net=$(ifconfig | grep -Eo "tun0")     
if [ "$(echo "$net")" != "" ]; then  
    echo ' <img src="../images/vpn_on.png" title="Conectado a servidor VPN" height="32" width="32">'
fi
net=$(ifconfig | grep -Eo "wlan0")     
if [ "$(echo "$net")" != "" ]; then  
    echo ' <img src="../images/wifi_on.png" title="Red inal&aacute;mbrica activada" height="32" width="32">'
fi
echo '<pre>'
echo '</pre>'
echo '</body>'
echo '</html>'
exit 0