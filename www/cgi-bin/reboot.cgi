#!/bin/sh

echo "Content-type: text/html"
echo ""

echo '<html>'
echo '<head>'
echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">'
echo '<title></title>'
echo '</head>'
echo '<body style="font-size:12pt;margin-left:20px;margin-top:20px">'
echo '<h1>REINICIANDO ROUTER...</h1>'
echo '<pre>'
reboot
echo '</pre>'
echo '<script type="text/javascript">'
echo '<!--'
echo 'window.location = "http://192.168.1.1"'
echo '//-->'
echo '</script>'
echo '</body>'
echo '</html>'

exit 0
