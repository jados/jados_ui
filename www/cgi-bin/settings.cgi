#!/bin/sh
echo "Content-type: text/html"
echo ""
cat <<EOF
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../css/estilo.css" media="screen">
<link rel="stylesheet" href="../css/onoff.css" media="screen">
<link rel="stylesheet" href="../css/pure-min.css" media="screen">
<script type="text/javascript" src="../js/jquery.js"></script> 
<script type="text/javascript" src="../js/menu.js"></script> 
<script type="text/javascript" src="../js/onoff.js"></script>
<script>
window.onload = detectarCarga;
 function detectarCarga(){
  document.getElementById("carga").style.display="none";
}
</script>
<script type="text/javascript">
function findselected(obj) { 
  var hab;
  var hp; 
  frm=obj.form; 
  num=obj.selectedIndex;
  if (num==0){
  	hab=false
	}
  if (num>1){
  	hab=true
	}
  frm.ipc.disabled=hab;  
  frm.ima.disabled=hab; 
  frm.pel.disabled=hab;
} 
</script>
</head>
<div id="carga" align="center">
  <img src="../images/loading.gif" />
</div>
EOF
###Carga de variables
device=$(uci get wireless.@wifi-iface[0].device)
wname=$(uci get wireless.@wifi-iface[0].ssid)
wpass=$(uci get wireless.@wifi-iface[0].key)
wencr=$(uci get wireless.@wifi-iface[0].encryption)
wmode=$(uci get wireless.radio0.hwmode)
wchan=$(uci get wireless.radio0.channel)

imode=$(cat /usr/imode)
iipad=$(uci get network.wan.ipaddr)
imasc=$(uci get network.wan.netmask)
igate=$(uci get network.wan.gateway)

lmasc=$(uci get network.lan.netmask)
lipad=$(uci get network.lan.ipaddr)
lgate=$(uci get network.lan.gateway)

lddns=$(uci get network.lan.dns)
ldns1=$(echo $lddns | cut -f1 -d ' ')
ldns2=$(echo $lddns | cut -f2 -d ' ')

sname=$(uci get system.@system[0].hostname)
dintp=$(uci get system.ntp.server)

wifi_list=$(iw dev wlan0 scan | grep 'BSS\|SSID\|channel\|signal\|WPA:\|RSN:\|WEP:' | sed s/'BSS '//g | sed s/'signal: '//g | sed s/'SSID: '//g | sed s/'(on wlan0)'//g | sed s/'DS Parameter set: channel '//g | sed s/'* Version: 1'//g | sed '/*/d')

###info######################################
dinamica="El tipo de conexi&oacute;n Din&aacute;mica es la m&aacute;s habitual. Si emplea un m&oacute;dem por cable, entonces dispondr&aacute; probablemente de una conexi&oacute;n din&aacute;mica. Si tiene un m&oacute;dem por cable o no est&aacute; seguro de su tipo de conexi&oacute;n, emplee &eacute;sta."
estatica="El tipo de conexi&oacute;n de Direcci&oacute;n IP est&aacute;tica es menos com&uacute;n que otros tipos de conexi&oacute;n. Utilice esta selecci&oacute;n s&oacute;lo si su ISP le ha proporcionado una direcci&oacute;n IP que no cambia nunca."
pppoe="PPPoE: Si emplea un módem DSL o su ISP le ha proporcionado una Nombre de usuario y una Contraseña, entonces su tipo de conexión es PPPoE. Utilice este tipo de conexión."

#############################################
gpio=$(ls /sys/class/gpio/ | grep -Eo "gpio*[0-9]")
usb=$(cat /sys/class/gpio/"$gpio"/value)
cat <<EOF
<body>
<ul id="nav">
<li><a href="#"><img src="../images/wifi.png" height="28" width="28"><strong> Red inal&aacute;mbrica</strong></a>
<ul>
<form action="set_config.cgi" method="post" class="pure-form pure-form-aligned">
    <fieldset>	
	<div class="pure-control-group">
        <label for="nombrered">Nombre red</label>
        <input type="text" value="$wname" name="nom" id="nombrered" style="width: 200px" required/>
	</div>
	<div class="pure-control-group">
	<label for="modfun">Modo de funcionamiento</label>
    <select id="modfun" name="mfu" class="pure-input-1-2" style="width: 200px" required>
EOF
	while read line
	do
    if [ $wmode == $line ]; then
        echo '<option value="'$line'" selected>'$line'</option>'
    else
    	echo '<option value="'$line'">'$line'</option>'
    fi
	done < wireless_modes
cat <<EOF
    </select>
	</div>
	
	<div class="pure-control-group">
	<label for="canal">Canal</label>
    <select id="canal" name="cha" class="pure-input-1-2" style="width: 200px" required>
EOF
	while read line
	do
    if [ $wchan == $line ]; then
    	echo '<option value="'$line'" selected>'$line'</option>'
    else
    	echo '<option value="'$line'">'$line'</option>'
    fi
	done < wireless_channels
cat <<EOF
    </select>
	</div>
		
	<div class="pure-control-group">
	<label for="tipseg">Tipo de seguridad</label>
    <select id="tipseg" name="tse" class="pure-input-1-2" style="width: 200px" required>
EOF
    if [ $wencr == "psk-mixed" ]; then
    	echo '<option value="psk-mixed" selected>WPA-PSK/WPA2-PSK</option>'
    else
        echo '<option value="psk-mixed">WPA-PSK/WPA2-PSK</option>'
    fi
    if [ $wencr == "psk2" ]; then
        echo '<option value="psk2" selected>WPA2-PSK</option>'
    else
    	echo '<option value="psk2">WPA2-PSK</option>'
    fi
    if [ $wencr == "psk" ]; then
    	echo '<option value="psk" selected>WPA-PSK</option>'
    else
    	echo '<option value="psk">WPA-PSK</option>'
    fi
    if [ $wencr == "none" ]; then
    	echo '<option value="none" selected>Sin seguridad</option>'
    else
    	echo '<option value="none">Sin seguridad</option>'
    fi
cat <<EOF
    </select>
	</div>
		
	<div class="pure-control-group">
	<label for="pass">Contrase&ntilde;a</label>
    <input type="password" value="$wpass" name="pas" id="pass"  style="width: 200px" pattern="[A-Za-z0-9]{8,63}" title ="Solo numeros y letras min = 8 max = 63 sin tildes o &ntilde " required/>
	</div>
	
	<div class="pure-controls">
    <button type="submit" class="pure-button pure-button-primary" style="width: 200px">Guardar</button>
	</div>
	
	</fieldset>
	</form>
</ul>

<li><a href="#"><img src="../images/internet.png" height="28" width="28"><strong> Conexi&oacute;n a Internet</strong></a>
<ul>
	<form action="set_config.cgi" method="post" class="pure-form pure-form-aligned">   
    
	<fieldset>
    
    <div class="pure-control-group">

 	<label for="tipconint">Tipo de conexi&oacute;n</label>   
    <select id="tipconint" name="tco" onchange="findselected(this)" class="pure-input-1-2" style="width: 200px" required>
EOF
    if [ $imode == "static" ]; then
    	echo '<option value="static" title="'$estatica'" selected>WAN Est&aacute;tica</option>'
    else
    	echo '<option value="static" title="'$estatica'">WAN Est&aacute;tica</option>'
    fi
    if [ $imode == "dhcp" ]; then
    	echo '<option value="dhcp" title="'$dinamica'" selected>WAN Din&aacute;mica</option>'
    else
    	echo '<option value="dhcp" title="'$dinamica'">WAN Din&aacute;mica</option>'
    fi
	cont=0
	printf %s "$wifi_list" | while IFS= read -r line
	do 
	let cont+=1
	det="0"
	line="$(echo $line | sed 's/^ *//')"
	if [ $(echo "$line" | grep -o ":" | wc -l) == 5 ]; then
		mac="$(echo $line | sed s/' '//g | sed s/':'/' '/g)"
		let pos2=cont+1
		let pos3=cont+2
		let pos4=cont+3
		let pos5=cont+4
		let pos6=cont+5
		det="0"
	fi
	if [ "$cont" = "$pos2" ]; then
		signal=$line
	fi
	if [ "$cont" = "$pos3" ]; then
		ssid=$line
	fi
	if [ "$cont" = "$pos4" ]; then
		channel=$line
	fi
	if [ "$cont" = "$pos5" ]; then
		if ( [ "$line" == "WPA:" ] || [ "$line" == "WEP:" ] || [ "$line" == "RSN:" ] ); then
			security=""
			if [ "$line" == "RSN:" ]; then
				security="psk2"
				det="1"
			fi
			if [ "$line" == "WPA:" ]; then
				security="psk"
				det="1"
			fi
		else
			security="none"
			det="1"
		fi
	fi	
	if [ "$det" == "1" ]; then
		det=0
		div="&&"
		if [ "$imode" == "$ssid" ]; then
			echo '<option value="'$mac$div$ssid$div$security$div$channel'" selected>Repetidor '$ssid  $signal'</option>'
		else
			echo '<option value="'$mac$div$ssid$div$security$div$channel'">Repetidor '$ssid  $signal'</option>'
		fi
    fi
	done
    ##<option title="$pppoe">PPPoE</option>
cat <<EOF
	</select>
	</div>
	
	<div class="pure-control-group">
	<label for="ip">Direcci&oacute;n IP</label>
    <input type="text" name="ipc" id="ip" value="$iipad" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" style="width: 200px" required/>
	</div>
	
	<div class="pure-control-group">
	<label for="mascara">M&aacute;scara de subred</label>
    <select id="mascara" name="ima" class="pure-input-1-2" style="width: 200px" required>
EOF
	while read line
	do
    if [ $imasc == $line ]; then
    	echo '<option value="'$line'" selected>'$line'</option>'
    else
    	echo '<option value="'$line'">'$line'</option>'
    fi
	done < local_netmask
cat <<EOF
    </select>
	</div>
	
	<div class="pure-control-group">
	<label for="puerta">Puerta de enlace</label>
    <input type="text" name="pel" id="puerta" value="$igate" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" style="width: 200px" required/>	
	</div>
	
	<div class="pure-controls">
	<button type="submit" class="pure-button pure-button-primary" style="width: 200px">Guardar</button>
	</div>
	
	</fieldset>
	</form>
</ul>

<li><a href="#"><img src="../images/network.png" height="28" width="28"><strong> Red Local</strong></a>
<ul>	
	<form action="set_config.cgi" method="post" class="pure-form pure-form-aligned">
	<fieldset>
		
	<div class="pure-control-group">
	<label for="dirip">Direcci&oacute;n IP</label>
    <input type="text" name="ipl" value="$lipad" id="dirip" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" style="width: 200px" required/>	
    </div>

	<div class="pure-control-group">
	<label for="mascaral">Mascara de subred</label>
    <select id="mascaral" name="msl" class="pure-input-1-2" style="width: 200px" required>
EOF
	while read line
	do
    if [ $lmasc == $line ]; then
    	echo '<option value="'$line'" selected>'$line'</option>'
    else
    	echo '<option value="'$line'">'$line'</option>'
    fi
	done < local_netmask
cat <<EOF
    </select>
	</div>

	<div class="pure-control-group">
	<label for="puertal">Puerta de enlace</label>
    <input type="text" name="pel" id="puertal" value="$lgate" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" style="width: 200px" required/>	
	</div>

	<div class="pure-control-group">
	<label for="dns">DNS primario</label>
    <input type="text" name="dd1" id="dnsp" value="$ldns1" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" style="width: 200px" required/>	
	</div>
	
	<div class="pure-control-group">
	<label for="dnss">DNS secundario</label>
    <input type="text" name="dd2" id="dnss" value="$ldns2" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" style="width: 200px" required/>	
	</div>
	
	<div class="pure-controls">
	<button type="submit" class="pure-button pure-button-primary" style="width: 200px">Guardar</button>
	</div>
	
    </fieldset>
	</form>
</ul>

<li><a href="#"><img src="../images/system.png" height="28" width="28"><strong> Sistema</strong></a>
<ul>	
	<form action="set_config.cgi" method="post" class="pure-form pure-form-aligned">
	<fieldset>

	<div class="pure-control-group">
	<label for="sys_name">Nombre de dispositivo</label>
    <input type="text" name="ndi" value="$sname" id="sys_name" style="width: 200px" required/>	
    </div>
	
	<div class="pure-control-group">
	<label for="ntp">Servidor NTP</label>
    <input type="text" name="ntp" value="$dintp" id="ntp" style="width: 200px" />	
	</div>
	
	<div class="pure-control-group">
	<label for="pad">Contrase&ntilde;a administrador</label>
    <input type="password" name="pad" value="$dintp" id="pad" style="width: 200px" />	
	</div>
	
	<div class="pure-controls">
	<button type="submit" class="pure-button pure-button-primary" style="width: 200px">Guardar</button>
	</div>
	
    </fieldset>
	</form>
</ul>
</body>
</html>
EOF
exit