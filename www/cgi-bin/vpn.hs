#!/usr/bin/haserl --upload-limit=20 --upload-dir=/tmp
content-type: text/html

<html>
<head>
<link rel="stylesheet" href="../css/pure-min.css">
<script type="text/javascript" src="../js/valida_vpn.js"></script>
</head>
<h5>En este apartado podra configurar su servidor VPN, cargue los archivos necesarios usando la opci&oacute;n cargar configuraci&oacute;n</h5>
<body>
<table class="pure-table pure-table-bordered" width="450">
   <thead>
      <tr>
         <th>#</th>
         <th>Lista de archivos almacenados<br>memoria en uso <% df / | awk '{ print $5 }' | tail -n 1 %></th>
		 <th>Acciones</th>
      </tr>
   </thead>
   <tbody>
      <% count=0 %>
      <% for i in $(ls /vpn); do %>
	  <% count=$(expr $count + 1) %> 
	  <% echo "<tr><td>$count</td><td>$i</td>" %>
	  <td><form action="set_config.cgi" class="pure-form pure-form-aligned" method=POST >
	  <% echo "<input type="hidden" name="dvp" value="$i">" %>
	  <button type="submit" class="pure-button pure-button-primary" style="width: 100px">Eliminar</button>
	  </form></td><tr>
	  <% done; %>
   </tbody>
</table>
<br>
<br>
<form action="<% echo -n $SCRIPT_NAME %>" class="pure-form pure-form-aligned" method=POST enctype="multipart/form-data" onsubmit="return Validate(this);">
<legend><strong>Cargar configuraci&oacute;n</strong></legend>
<fieldset>
<input type="file" id="file" name="uploadfile" style="width: 250px">
<input type="submit" value="Cargar" class="pure-button pure-button-primary" style="width: 200px">
</fieldset>
</form>
<% net=$(ifconfig | grep -Eo "tun0") %>
<% if [ "$(echo "$net")" != "" ]; then %>
<form action="set_config.cgi" class="pure-form pure-form-aligned" method=POST >
<legend><strong>Conexi&oacute;n VPN activa</strong></legend>
<h4>IP <% /etc/daemon/get_public_ip %></h4>
<fieldset>
<input type="hidden" name="svp" value="stop">
<button type="submit" class="pure-button pure-button-primary" style="width: 200px">Detener conexi&oacute;n</button>
</fieldset>
</form>
<% else %>
<form action="set_config.cgi" class="pure-form pure-form-aligned" method=POST >
<legend><strong>Configuraci&oacute;n VPN predeterminada</strong></legend>
<fieldset>
<select id="vpn" name="vpn" style="width: 250px">
<% for l in $(ls /vpn | grep '.ovpn'); do %>
<% if [ "$l" == $(cat /usr/vpn) ]; then %>
<% echo "<option selected>$l</option>" %>
<% else %>
<% echo "<option>$l</option>" %>
<% fi %>
<% done; %>
</select>
<button type="submit" class="pure-button pure-button-primary" style="width: 200px">Seleccionar y Conectar</button>
</fieldset>
</form>
<% fi %>

<% if test -n "$HASERL_uploadfile_path"; then %>
<h4>Archivo enviado: <% echo -n $FORM_uploadfile_name %></h4>
<% mv -f "$HASERL_uploadfile_path" /vpn/"$FORM_uploadfile_name" %>
<script type="text/javascript">
<!--
window.location = "vpn.hs"
//-->
</script>
<% fi %>
</body>
</html>